module.exports = {
  purge: ["./src/**/*.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'logo': ['Shrikhand'],
      'serif': ['Hind'],
    },
    extend: {
      container: {
        center: true,
        padding: "1rem"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
